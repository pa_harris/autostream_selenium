﻿using autostream.Helpers;
using autostream.Models;
using autostream.Views;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace autostream
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        private static ReaderWriterLockSlim lock_ = new ReaderWriterLockSlim();

        private void WriteLog(string log, string filepath)
        {
            try
            {
                if (!Directory.Exists(System.IO.Path.GetDirectoryName(filepath)))
                {
                    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(filepath));
                }
            }
            catch (Exception ee)
            {

            }

            if (!System.IO.File.Exists(filepath))
            {
                lock_.EnterWriteLock();
                try
                {
                    // Create a file to write to.
                    using (StreamWriter sw = System.IO.File.CreateText(filepath))
                    {
                        sw.WriteLine(log);
                    }
                }
                finally
                {
                    lock_.ExitWriteLock();
                }                
            }
            else
            {
                lock_.EnterWriteLock();
                try
                {
                    using (StreamWriter sw = System.IO.File.AppendText(filepath))
                    {
                        sw.WriteLine(log);
                    }
                }
                finally
                {
                    lock_.ExitWriteLock();
                }                
            }
        }

        #region Properties
        DispatcherTimer timer;
        string message = "";
        private ObservableCollection<StreamModel> _ListThreads = new ObservableCollection<StreamModel>();
        public ObservableCollection<StreamModel> ListThreads { get => _ListThreads; set { _ListThreads = value; OnPropertyChanged(); } }
        #endregion

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            FirstLoad();
        }
        private void FirstLoad()
        {
            ListThreads.Clear();
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();

            if (System.IO.File.Exists(constants.filePathListThreads))
            {
                var lstItem = File.ReadAllLines(constants.filePathListThreads);
                foreach (var item in lstItem)
                {
                    var data = item.Split('|');

                    StreamModel model = new StreamModel();

                    model.Id = Convert.ToInt32(data.ElementAt(0));
                    model.Title = data.ElementAt(1);
                    model.Input = data.ElementAt(2);
                    model.DateTimeSelected = DateTime.ParseExact(data.ElementAt(3), "dd/MM/yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);
                    model.StreamUrl = data.ElementAt(4);
                    model.StreamKey = data.ElementAt(5);
                    model.Description = data.ElementAt(6);
                    model.Profile = new Profile { ProfileName = data.ElementAt(7) };
                    model.VideoUrl = data.ElementAt(8);
                    model.BuyView = Convert.ToInt32(data.ElementAt(9));

                    ListThreads.Add(model);
                }
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            var lstThreadStarted = ListThreads.Where((o) => (o.Status == EStatus.Started && Math.Abs((DateTime.Now - o.DateTimeSelected).TotalMinutes) <= constants.PreviewMinute)
            || (o.Status == EStatus.Streaming) && Math.Abs((DateTime.Now - o.DateTimeSelected).TotalMinutes) <= 1
            || (o.Status == EStatus.Streamed && Math.Abs((DateTime.Now - o.DateTimeEndSelected).TotalMinutes) <= 1)).ToList();
            foreach (var item in lstThreadStarted)
            {
                if (item.Status == EStatus.Streaming && Math.Abs((DateTime.Now - item.DateTimeSelected).TotalMinutes) <= 1)
                {
                    // golive 
                    GoliveItem(item);
                    continue;
                }
                else if (item.Status == EStatus.Started && Math.Abs((DateTime.Now - item.DateTimeSelected).TotalMinutes) <= constants.PreviewMinute)
                {
                    // tạo live stream
                    item.thread = new System.Threading.Thread(() =>
                    {
                        item.Status = EStatus.Streaming;
                        StartStreamingItem(item);
                    });
                    item.thread.IsBackground = true;
                    item.thread.Start();
                } 
                else if (item.Status == EStatus.Streamed && Math.Abs((DateTime.Now - item.DateTimeEndSelected).TotalMinutes) <= 1)
                {
                    // end
                    StopItem(item);
                    ResetFileThread();
                    continue;
                }
            }
        }


        private void AddThreadsClick(object sender, RoutedEventArgs e)
        {
            AddThreadsDiaglog addThreadsDiaglog = new AddThreadsDiaglog();
            addThreadsDiaglog.Owner = this;
            addThreadsDiaglog.ReturnFunction += AddThreadsDiaglog_ReturnFunction;
            addThreadsDiaglog.ShowDialog();
        }

        private void AddThreadsDiaglog_ReturnFunction(StreamModel obj)
        {
            if (obj != null)
            {
                obj.Id = (ListThreads.Count() != 0) ? ListThreads.Select(x => x.Id).Max() + 1 : 1;
                ListThreads.Add(obj);
                ResetFileThread();
            }
        }

        private void StartItem_Click(object sender, RoutedEventArgs e)
        {
            StreamModel item = (sender as Button).DataContext as StreamModel;
            StartItem(item);
            ResetFileThread();
        }

        private void StopItem(StreamModel model)
        {
            if (model.thread == null)
            {
                return;
            }
            if ((model.thread.ThreadState & (System.Threading.ThreadState.Stopped | System.Threading.ThreadState.Unstarted)) == 0)
            {
                StopAThread(model);
                return;
            }
            if (model.thread.ThreadState == System.Threading.ThreadState.WaitSleepJoin)
            {
                model.thread.Interrupt();
                StopAThread(model);
                return;
            }
            if (model.thread.ThreadState == System.Threading.ThreadState.Suspended || model.thread.ThreadState == System.Threading.ThreadState.SuspendRequested)
            {
                model.thread.Resume();
                StopAThread(model);
                return;
            }
            if (model.thread.ThreadState == System.Threading.ThreadState.Stopped)
            {
                StopAThread(model);
            }
        }

        private void StartItem(StreamModel model)
        {
            if (model == null)
            {
                return;
            }
            if (model.Status == EStatus.Streaming || model.Status == EStatus.Streamed)
            {
                return;
            }
            // nếu có stream url nghĩa là đã tạo video youtube rồi
            if (!string.IsNullOrEmpty(model.VideoUrl))
            {
                model.Status = EStatus.Started;
                return;
            }

            if (model?.thread != null)
            {
                while (model.thread.ThreadState != System.Threading.ThreadState.Stopped)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(0.5));
                }
            }

            // thread đã stop rồi
            model.thread = new System.Threading.Thread(() =>
            {
                CreateStreamVideo(model);
                model.Status = EStatus.Started;
            });
            model.thread.IsBackground = true;
            model.thread.Start();
        }

        private void StopItem_Click(object sender, RoutedEventArgs e)
        {
            StreamModel item = (sender as Button).DataContext as StreamModel;
            Thread t = new Thread(() =>
            {                
                StopItem(item);
                ResetFileThread();
            });
            t.IsBackground = true;
            t.Start();
        }
        private void StartAll_Click(object sender, RoutedEventArgs e)
        {
            Thread t = new Thread(() => 
            {
                foreach (var item in ListThreads)
                {
                    StartItem(item);
                    ResetFileThread();
                }
            });
            t.IsBackground = true;
            t.Start();
        }
        private void StopAll_Click(object sender, RoutedEventArgs e)
        {
            Thread t = new Thread(() =>
            {
                foreach (var item in ListThreads)
                {
                    StopItem(item);
                }
                ResetFileThread();
            });
            t.IsBackground = true;
            t.Start();
        }

        private void CreateStreamVideo(StreamModel model)
        {
            if (model != null && model.Status != EStatus.Started)
            {
                model.chromeDriver = StartChromeDriver(model.Profile.ProfileFullPath);

                #region Click create
                bool isClickedCreate = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.Id("create-icon"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
                if (!isClickedCreate)
                {
                    WriteLog(message, $"Logs\\{model.Profile.ProfileName}.txt");
                    return;
                }
                #endregion

                #region Click Go live
                bool isClickedGoLive = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.Id("text-item-1"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
                if (!isClickedGoLive)
                {
                    WriteLog(message, $"Logs\\{model.Profile.ProfileName}.txt");
                    return;
                }
                #endregion
                
                #region Click Manage
                Thread.Sleep(TimeSpan.FromSeconds(7));
                bool isClickedManage = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.Id("feature-item-2"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
                if (!isClickedManage)
                {
                    WriteLog(message, $"Logs\\{model.Profile.ProfileName}.txt");
                    return;
                }
                #endregion

                #region Click Schedule Stream
                Thread.Sleep(TimeSpan.FromSeconds(2));
                bool isClickedScheduleStream = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.Id("schedule-button"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
                if (!isClickedScheduleStream)
                {
                    WriteLog(message, $"Logs\\{model.Profile.ProfileName}.txt");
                    return;
                }
                #endregion

                #region Click Create New
                bool isClickedCreateNew = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.XPath("/html/body/ytcp-app/ytls-popup-container/paper-dialog/ytls-duplicate-broadcast-renderer/div[4]/div[1]/ytls-button-renderer/a/paper-button/yt-formatted-string"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
                if (!isClickedCreateNew)
                {
                    WriteLog(message, $"Logs\\{model.Profile.ProfileName}.txt");
                    //return;
                }
                #endregion

                #region Sendkey Title
                bool isSentKeyTitle = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.XPath("/html/body/ytcp-app/ytls-popup-container/paper-dialog/ytls-broadcast-setup-renderer/div/div[1]/div[1]/ytls-metadata-collection-renderer/div[2]/ytls-metadata-control-renderer[1]/div/ytls-mde-title-renderer/paper-input/paper-input-container/div[2]/div/iron-input/input"), SeleniumHelper.ESeleniumElementAction.SendKey, model.Title, out message);
                if (!isSentKeyTitle)
                {
                    WriteLog(message, $"Logs\\{model.Profile.ProfileName}.txt");
                    //return;
                }
                #endregion

                #region Sendkey Description
                bool isSentKeyDescription = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.XPath("/html/body/ytcp-app/ytls-popup-container/paper-dialog/ytls-broadcast-setup-renderer/div/div[1]/div[1]/ytls-metadata-collection-renderer/div[2]/ytls-metadata-control-renderer[3]/div/ytls-mde-description-renderer/paper-textarea/paper-input-container/div[2]/div/iron-autogrow-textarea/div[2]/textarea"), SeleniumHelper.ESeleniumElementAction.SendKey, model.Description, out message);
                if (!isSentKeyDescription)
                {
                    WriteLog(message, $"Logs\\{model.Profile.ProfileName}.txt");
                    return;
                }
                #endregion

                #region Select datetime
                //IJavaScriptExecutor js = (IJavaScriptExecutor)model.chromeDriver;
                //// select year
                //bool isClickedDate = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.Id("input-22"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
                //if (!isClickedDate)
                //{
                //    WriteLog(message, $"Logs\\{model.ProfileName}.txt");
                //    return;
                //}
                //bool isClickedYear = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.Id("showSelectedYear"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
                //if (!isClickedYear)
                //{
                //    WriteLog(message, $"Logs\\{model.ProfileName}.txt");
                //    return;
                //}
                //string className = "each-list-of-years style-scope app-datepicker";
                //int value = model.DateTimeSelected.Year;
                //string jsSelectDateTime = $"(Array.from(document.getElementsByClassName('{className}')).filter(field => field.getAttribute('aria-label') == {value})[0]).click()";
                //js.ExecuteScript(jsSelectDateTime);

                //// select month
                //isClickedDate = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.Id("input-22"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
                //if (!isClickedDate)
                //{
                //    WriteLog(message, $"Logs\\{model.ProfileName}.txt");
                //    return;
                //}

                //string s = "";
                #endregion

                #region Click make for kids
                bool isClickedForKid = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.XPath("/html/body/ytcp-app/ytls-popup-container/paper-dialog/ytls-broadcast-setup-renderer/div/div[1]/div[1]/ytls-metadata-collection-renderer/div[2]/ytls-metadata-control-renderer[8]/div/ytls-audience-selection-renderer/ytcp-audience-picker/div[4]/paper-radio-group/paper-radio-button[1]/div[1]/div[1]"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
                if (!isClickedForKid)
                {
                    WriteLog(message, $"Logs\\{model.Profile.ProfileName}.txt");
                    return;
                }
                #endregion

                #region Click Create Stream
                bool isClickedCreateStream = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.XPath("/html/body/ytcp-app/ytls-popup-container/paper-dialog/ytls-broadcast-setup-renderer/div/div[1]/div[2]/div/ytls-button-renderer/a/paper-button/yt-formatted-string"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
                if (!isClickedCreateStream)
                {
                    WriteLog(message, $"Logs\\{model.Profile.ProfileName}.txt");
                    return;
                }
                #endregion

                #region Load Data
                Thread.Sleep(TimeSpan.FromSeconds(7));

                #region Go to video link
                IWebElement itemVideo = null;
                try
                {
                    itemVideo = model.chromeDriver.FindElement(By.XPath("/html/body/ytcp-app/ytls-live-streaming-section/ytls-core-app/div/div[2]/div/ytls-live-dashboard-page-renderer/div[1]/div[2]/ytls-broadcast-list/ytls-broadcast-list-content/div[2]/div/div/ytcp-video-row[3]/div/div[2]/ytcp-video-list-cell-video/div[2]/div[1]/h3/a"));
                }
                catch (Exception ee)
                {
                }
                if (itemVideo == null)
                {
                }
                else
                {
                    // navigate to video
                    string jsClickStreamVideo = $"(Array.from(document.getElementsByClassName('video-title-wrapper')).filter(field => field.innerText == '{model.Title}')[0]).childNodes[2].click()";
                    IJavaScriptExecutor js = model.chromeDriver as IJavaScriptExecutor;
                    // javascript cần return giá trị.
                    var dataFromJS = (string)js.ExecuteScript(jsClickStreamVideo);

                }
                #endregion

                #region Click Copy StreamKey
                Thread.Sleep(TimeSpan.FromSeconds(2));
                bool isClickedCopyStreamKey = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.XPath("/html/body/ytcp-app/ytls-live-streaming-section/ytls-core-app/div/div[2]/div/ytls-live-dashboard-page-renderer/div[1]/div[1]/ytls-live-control-room-renderer/div[1]/div[2]/ytls-live-dashboard-widget-renderer/iron-pages/ytls-stream-settings-widget-renderer/div/ytls-metadata-collection-renderer[1]/div[2]/ytls-metadata-control-renderer[3]/div/ytls-ingestion-settings-item-renderer/div[2]/div[3]/ytls-button-renderer/a/paper-button/yt-formatted-string"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
                if (!isClickedCopyStreamKey)
                {
                    WriteLog(message, $"Logs\\{model.Profile.ProfileName}.txt");
                    return;
                }
                #endregion

                model.StreamKey = GetDataInClipboard();

                #region Click Copy StreamUrl
                bool isClickedCopyStreamUrl = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.XPath("/html/body/ytcp-app/ytls-live-streaming-section/ytls-core-app/div/div[2]/div/ytls-live-dashboard-page-renderer/div[1]/div[1]/ytls-live-control-room-renderer/div[1]/div[2]/ytls-live-dashboard-widget-renderer/iron-pages/ytls-stream-settings-widget-renderer/div/ytls-metadata-collection-renderer[1]/div[2]/ytls-metadata-control-renderer[4]/div/ytls-ingestion-settings-item-renderer/div[2]/div[3]/ytls-button-renderer/a/paper-button/yt-formatted-string"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
                if (!isClickedCopyStreamUrl)
                {
                    WriteLog(message, $"Logs\\{model.Profile.ProfileName}.txt");
                    return;
                }
                #endregion

                model.StreamUrl = GetDataInClipboard();
                #endregion

                model.VideoUrl = model.chromeDriver.Url;

                model.Status = EStatus.Started;

                model.chromeDriver.Close();
                model.chromeDriver.Quit();

                ResetFileThread();
            }
        }

        private void StopAThread(StreamModel model)
        {
            if (model?.thread != null)
            {
                try
                {
                    model.thread.Abort();
                }
                catch (Exception ee)
                {
                }
                try
                {
                    // Click end stream
                    if (model.Status == EStatus.Streamed)
                    {
                        // cần click end stream
                        #region Click End Stream
                        bool isClickedEndStream = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.Id("end-stream-button"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
                        if (!isClickedEndStream)
                        {
                            WriteLog(message, $"Logs\\{model.Profile.ProfileName}.txt");
                            //return;
                        }
                        #endregion

                        #region Click End
                        bool isClickedEnd = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.XPath("/html/body/ytcp-confirmation-dialog/ytcp-dialog/paper-dialog/div[3]/div[2]/ytcp-button[2]"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
                        if (!isClickedEnd)
                        {
                            WriteLog(message, $"Logs\\{model.Profile.ProfileName}.txt");
                            return;
                        }
                        #endregion
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(5));
                    model.chromeDriver.Close();
                    model.chromeDriver.Quit();
                }
                catch (Exception ee)
                {

                }
                try
                {
                    if (model.cmd != null)
                    {
                        model.cmd.StandardInput.WriteLine("q");
                        model.cmd.StandardInput.Flush();
                    }
                }
                catch (Exception ee)
                {
                }
                try
                {
                    model.cmd.CloseMainWindow();
                    model.cmd.Close();
                    model.cmd.Dispose();
                }
                catch (Exception ee)
                {

                }
            }
            if (model.Status == EStatus.Streaming || model.Status == EStatus.Streamed || model.Status == EStatus.Started)
            {
                model.Status = EStatus.None;
            }
        }

        private string GetDataInClipboard()
        {
            try
            {
                string clipboardData = null;
                Exception threadEx = null;
                Thread staThread = new Thread(
                    delegate ()
                    {
                        try
                        {
                            clipboardData = Clipboard.GetText(TextDataFormat.Text);
                        }

                        catch (Exception ex)
                        {
                            threadEx = ex;
                        }
                    });
                staThread.SetApartmentState(ApartmentState.STA);
                staThread.Start();
                staThread.Join();
                return clipboardData;
            }
            catch (Exception exception)
            {
                return string.Empty;
            }
        }

        private void StartStreamingItem(StreamModel model)
        {
            #region Open Chrome Driver
            if (string.IsNullOrEmpty(model.VideoUrl))
            {
                WriteLog("Video url trống", $"Logs\\{model.Profile.ProfileName}.txt");
                return;
            }
#if DEBUG
            model.chromeDriver = new ChromeDriver(@"C:\Users\admin\Downloads", GetChromeOption(model.Profile.ProfileFullPath));
#else
            model.chromeDriver = new ChromeDriver(GetChromeOption(model.Profile.ProfileFullPath));
#endif
            model.chromeDriver.Url = model.VideoUrl;
            model.chromeDriver.Navigate();
            #endregion

            string cmdCommand = $"ffmpeg -stream_loop -1 -re -i {model.Input} -c:v libx264 -preset ultrafast -r 30 -g 60 -b:v 1500k -c:a aac -threads 6 -ar 44100 -b:a 128k -bufsize 512k -pix_fmt yuv420p -f flv {model.StreamUrl}/{model.StreamKey}";

            model.cmd = new Process();

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "cmd.exe";
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardInput = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;

            model.cmd.StartInfo = startInfo;
            model.cmd.Start();

            model.cmd.StandardInput.WriteLine(cmdCommand);
            model.cmd.StandardInput.Flush();
            model.cmd.StandardInput.Close();

            model.cmd.ErrorDataReceived += (s, args) =>
            {
                string error = args.Data;
                
                this.Dispatcher.Invoke(() =>
                {
                    try
                    {
                        model.FPS = Convert.ToInt32(Regex.Match(error, @"(?<=fps=).*?(?=.q=)", RegexOptions.Singleline).ToString().Trim());
                        model.Bitrate = Convert.ToDouble(Regex.Match(error, @"(?<=bitrate=).*?(?=kbits/s)", RegexOptions.Singleline).ToString().Trim());
                        model.Speed = Convert.ToDouble(Regex.Match(error, @"(?<=speed=).*?(?=x)", RegexOptions.Singleline).ToString().Trim());
                    }
                    catch (Exception ee)
                    {
                        WriteLog(ee.Message, $"Logs\\{model.Profile.ProfileName}.txt");
                    }
                });
            };

            model.cmd.BeginErrorReadLine();
            // cmd.WaitForExit();
        }

        private void GoliveItem(StreamModel model)
        {
            #region Click Go Live
            bool isClickedGoLive = SeleniumHelper.FindElementAndDoAction(model.chromeDriver, By.Id("start-stream-button"), SeleniumHelper.ESeleniumElementAction.Click, null, out message);
            if (!isClickedGoLive)
            {
                WriteLog(message, $"Logs\\{model.Profile.ProfileName}.txt");
                return;
            }
            #endregion
            model.Status = EStatus.Streamed;
        }

        // Start chrome driver để tạo live stream
        private ChromeDriver StartChromeDriver(string ProfilePath)
        {
            ChromeDriver driver = null;            

#if DEBUG
            driver = new ChromeDriver(@"C:\Users\admin\Downloads", GetChromeOption(ProfilePath));
#else
            driver = new ChromeDriver(GetChromeOption(ProfilePath));
#endif

            try
            {
                driver.Url = $"https://studio.youtube.com";
                driver.Navigate();
            }
            catch (Exception ee)
            {
            }

            return driver;
        }

        private ChromeOptions GetChromeOption(string ProfileFullPath)
        {
            ChromeOptions options = new ChromeOptions();

            if (Directory.Exists(ProfileFullPath))
            {
                options.AddArguments("user-data-dir=" + ProfileFullPath);
            }

            options.AddArgument("--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36");
            options.AddArgument("--disable-notifications");
            options.AddArgument("--disable-web-security");
            options.AddArgument("--disable-blink-features");
            options.AddArgument("--disable-blink-features=AutomationControlled");
            options.AddArgument("--disable-infobars");
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("--allow-running-insecure-content");
            options.AddArgument("--no-sandbox");
            options.AddAdditionalCapability("useAutomationExtension", false);
            options.AddExcludedArgument("enable-automation");
            options.AddExcludedArgument("headless");
            options.AddUserProfilePreference("credentials_enable_service", false);
            options.AddUserProfilePreference("profile.password_manager_enabled", false);

            return options;
        }

        private void RemoveItem_Click(object sender, RoutedEventArgs e)
        {
            StreamModel item = (sender as Button).DataContext as StreamModel;
            ListThreads.Remove(item);
            ResetFileThread();
        }

        private void EditItem_Click(object sender, RoutedEventArgs e)
        {
            StreamModel item = (sender as Button).DataContext as StreamModel;
            AddThreadsDiaglog addThreadsDiaglog = new AddThreadsDiaglog(item);
            addThreadsDiaglog.Owner = this;
            addThreadsDiaglog.ReturnFunction += EditThreadsDiaglog_ReturnFunction;
            addThreadsDiaglog.ShowDialog();
        }

        private void EditThreadsDiaglog_ReturnFunction(StreamModel obj)
        {
            if (obj != null)
            {
                int index = ListThreads.ToList().FindIndex(x => x.Id == obj.Id);
                if (index >= 0 && index < ListThreads.Count())
                {
                    ListThreads[index] = obj;
                    ResetFileThread();
                }
            }
        }

        private void ResetFileThread()
        {
            File.WriteAllText(constants.filePathListThreads, string.Empty);
            foreach (var item in ListThreads)
            {
                WriteLog(item.GetDataToFile(), constants.filePathListThreads);
            }
        }

        private void AddProfileClick(object sender, RoutedEventArgs e)
        {
            ProfileTable profileTable = new ProfileTable();
            profileTable.Owner = this;
            profileTable.ShowDialog();
        }

        private void BuyView_Click(object sender, RoutedEventArgs e)
        {
            BuyView buyView = new BuyView();
            buyView.Owner = this;
            buyView.NotifyToMain += BuyView_NotifyToMain;
            buyView.ShowDialog();
        }

        private void BuyView_NotifyToMain(StreamModel thread, int quantity)
        {
            var item = ListThreads.FirstOrDefault((o) => o.Id == thread.Id);
            item.BuyView += quantity;
            ResetFileThread();
        }
    }
}