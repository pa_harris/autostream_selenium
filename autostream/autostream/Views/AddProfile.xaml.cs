﻿using autostream.Helpers;
using autostream.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace autostream.Views
{
    /// <summary>
    /// Interaction logic for AddProfile.xaml
    /// </summary>
    public partial class AddProfile : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private string _ProfileName;
        public string ProfileName { get => _ProfileName; set { _ProfileName = value; OnPropertyChanged(); } }

        public AddProfile()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateData())
            {
                Profile profile = new Profile { ProfileName = this.ProfileName };
                CreateProfile(profile.ProfileFullPath);
            }
        }

        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(ProfileName))
            {
                MessageBox.Show("Profile name không được trống!", "Cảnh báo");
                return false;
            }
            string[] s = Directory.GetDirectories(constants.profileFolderPath);
            if (s.Contains($"Profile\\{ProfileName}"))
            {
                MessageBox.Show("Profile đã tồn tại!", "Cảnh báo");
                return false;
            }
            return true;
        }
        private void CreateProfile(string ProfilePath)
        {
            string chromex64 = @"C:\Program Files\Google\Chrome\Application\chrome.exe";
            string chromex86 = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe";
            Process chrome = new Process();

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = chromex86;
            startInfo.Arguments = $"\"--user-data-dir=\"{ProfilePath}\""; ;
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            chrome.StartInfo = startInfo;

            try
            {
                chrome.Start();
            }
            catch (Exception ee)
            {
                startInfo.FileName = chromex64;
                chrome.Start();
            }
            
            chrome.WaitForExit();
            chrome.Close();
        }
    }
}
