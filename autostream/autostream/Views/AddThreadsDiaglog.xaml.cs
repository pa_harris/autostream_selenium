﻿using autostream.Helpers;
using autostream.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace autostream.Views
{
    /// <summary>
    /// Interaction logic for AddThreadsDiaglog.xaml
    /// </summary>
    public partial class AddThreadsDiaglog : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #region Properties
        private int _Id = -1;
        public int Id { get => _Id; set { _Id = value; OnPropertyChanged(); } }

        private string _ThreadTitle = "";
        public string ThreadTitle { get => _ThreadTitle; set { _ThreadTitle = value; OnPropertyChanged(); } }

        private string _FileFullPath = "";
        public string FileFullPath { get => _FileFullPath; set { _FileFullPath = value; OnPropertyChanged(); } }

        private EServer _ServerSelected;
        public EServer ServerSelected { get => _ServerSelected; set { _ServerSelected = value; OnPropertyChanged(); } }

        private string _InputTypeSelected;
        public string InputTypeSelected { get => _InputTypeSelected; set { _InputTypeSelected = value; OnPropertyChanged(); } }
        
        private Profile _ProfileSelected;
        public Profile ProfileSelected { get => _ProfileSelected; set { _ProfileSelected = value; OnPropertyChanged(); } }
        
        private string _Description;
        public string Description { get => _Description; set { _Description = value; OnPropertyChanged(); } }

        private DateTime _DateTimeSelected = DateTime.Now;
        public DateTime DateTimeSelected { get => _DateTimeSelected; set { _DateTimeSelected = value; OnPropertyChanged(); } }

        private DateTime _DateTimeEndSelected = DateTime.Now;
        public DateTime DateTimeEndSelected { get => _DateTimeEndSelected; set { _DateTimeEndSelected = value; OnPropertyChanged(); } }

        private byte _Hour = 0;
        public byte Hour { get => _Hour; set { _Hour = value; OnPropertyChanged(); } }

        private byte _Minute = 0;
        public byte Minute { get => _Minute; set { _Minute = value; OnPropertyChanged(); } }

        private byte _HourEnd = 0;
        public byte HourEnd { get => _HourEnd; set { _HourEnd = value; OnPropertyChanged(); } }

        private byte _MinuteEnd = 0;
        public byte MinuteEnd { get => _MinuteEnd; set { _MinuteEnd = value; OnPropertyChanged(); } }

        public ObservableCollection<Profile> ListProfile { get; set; } = (new ObservableCollection<Profile>());
        public ObservableCollection<EServer> ListServer { get; set; } = (new ObservableCollection<EServer>());
        public ObservableCollection<string> ListInputType { get; set; } = (new ObservableCollection<string>());

        public event Action<StreamModel> ReturnFunction;
        #endregion
        public AddThreadsDiaglog()
        {
            InitializeComponent();
            DataContext = this;
            FirstLoad();
        }
        public AddThreadsDiaglog(StreamModel model)
        {
            InitializeComponent();
            DataContext = this;
            FirstLoad();

            #region Load data
            Id = model.Id;
            ThreadTitle = model.Title;
            FileFullPath = model.Input;
            ServerSelected = model.Server;
            ProfileSelected = model.Profile;
            Description = model.Description;
            DateTimeSelected = model.DateTimeSelected.Date;
            DateTimeEndSelected = model.DateTimeEndSelected.Date;
            Hour = (byte)model.DateTimeSelected.Hour;
            Minute = (byte)model.DateTimeSelected.Minute;
            HourEnd = (byte)model.DateTimeEndSelected.Hour;
            MinuteEnd = (byte)model.DateTimeEndSelected.Minute;
            #endregion

            btnAdd.Content = "Cập nhật";
        }

        private void FirstLoad()
        {
            ListServer.Add(EServer.Youtube);
            ListInputType.Add("File");
            ServerSelected = ListServer.FirstOrDefault();
            InputTypeSelected = ListInputType.FirstOrDefault();
            LoadProfile();
        }

        private void FileOpen(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false;
            if (openFileDialog.ShowDialog() == true)
            {
                FileFullPath = openFileDialog.FileName;
            }
        }

        private void Add(object sender, RoutedEventArgs e)
        {
            if (ValidateData())
            {
                ReturnFunction(new StreamModel
                {
                    Id = this.Id,
                    Title = ThreadTitle,
                    Input = FileFullPath,
                    Server = ServerSelected,
                    Profile = this.ProfileSelected,
                    Description = this.Description,
                    DateTimeSelected = new DateTime(this.DateTimeSelected.Year, this.DateTimeSelected.Month, this.DateTimeSelected.Day, Hour, Minute, 0),
                    DateTimeEndSelected = new DateTime(this.DateTimeEndSelected.Year, this.DateTimeEndSelected.Month, this.DateTimeEndSelected.Day, HourEnd, MinuteEnd, 0)
                });

                if (btnAdd.Content == "Cập nhật")
                {
                    this.Close();
                }
            }
        }

        private void CancelAdd(object sender, RoutedEventArgs e)
        {
            ReturnFunction(null);
            this.Close();
        }

        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(ThreadTitle))
            {
                MessageBox.Show("Tên luồng không được trống!", "Cảnh báo");
                return false;
            }
            if (string.IsNullOrEmpty(InputTypeSelected))
            {
                MessageBox.Show("Kiểu đầu vào không được trống!", "Cảnh báo");
                return false;
            }
            if (string.IsNullOrEmpty(FileFullPath))
            {
                MessageBox.Show("File đầu vào không được trống!", "Cảnh báo");
                return false;
            }
            if (string.IsNullOrEmpty(ServerSelected.ToString()))
            {
                MessageBox.Show("Máy chủ không được trống!", "Cảnh báo");
                return false;
            }
            if (ProfileSelected == null)
            {
                MessageBox.Show("Profile không được trống!", "Cảnh báo");
                return false;
            }
            if (new DateTime(DateTimeSelected.Year, DateTimeSelected.Month, DateTimeSelected.Day, this.Hour, this.Minute, 0) < DateTime.Now)
            {
                MessageBox.Show("Không thể live trong quá khứ!", "Cảnh báo");
                return false;
            }
            if (new DateTime(DateTimeEndSelected.Year, DateTimeEndSelected.Month, DateTimeEndSelected.Day, this.HourEnd, this.MinuteEnd, 0) < DateTime.Now)
            {
                MessageBox.Show("Không thể end live trong quá khứ!", "Cảnh báo");
                return false;
            }
            if ((Hour > 23 || Hour < 0) || (HourEnd > 23 || HourEnd < 0))
            {
                MessageBox.Show("Giờ không hợp lệ! Giờ hợp lệ từ 0h đến 23h", "Cảnh báo");
                return false;
            }
            if ((Minute > 59 || Minute < 0 || Minute % 5 != 0) || (MinuteEnd > 59 || MinuteEnd < 0))
            {
                MessageBox.Show("Phút không hợp lệ! Phút hợp lệ từ 0p đến 59p và chia hết cho 5!", "Cảnh báo");
                return false;
            }
            if (DateTime.Now > (new DateTime(2021, 2, 17)).AddDays(7))
            {
                this.Close();
                return false;
            }
            return true;
        }

        private void LoadProfile()
        {
            string[] s = Directory.GetDirectories(constants.profileFolderPath);

            foreach (var item in s)
            {
                ListProfile.Add(new autostream.Models.Profile { ProfileName = item.Replace($"{constants.profileFolderPath}\\", "") });
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            ReturnFunction(null);
        }
    }
}
