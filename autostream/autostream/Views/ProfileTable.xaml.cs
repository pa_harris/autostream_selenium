﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace autostream.Views
{
    /// <summary>
    /// Interaction logic for ProfileTable.xaml
    /// </summary>
    public partial class ProfileTable : Window
    {
        public ProfileTable()
        {
            InitializeComponent();
        }

        private void AddProfile(object sender, RoutedEventArgs e)
        {
            AddProfile addProfile = new AddProfile();
            addProfile.Owner = this;
            addProfile.ShowDialog();
        }

        private void CheckProfileItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void RemoveProfileItem_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
