﻿using autostream.Helpers;
using autostream.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace autostream.Views
{
    /// <summary>
    /// Interaction logic for BuyView.xaml
    /// </summary>
    public partial class BuyView : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #region Properties
        private ObservableCollection<StreamModel> _ListThreads = new ObservableCollection<StreamModel>();
        public ObservableCollection<StreamModel> ListThreads { get => _ListThreads; set { _ListThreads = value; OnPropertyChanged(); } }

        private ObservableCollection<ViewService> _ViewServices = new ObservableCollection<ViewService>();
        public ObservableCollection<ViewService> ViewServices { get => _ViewServices; set { _ViewServices = value; OnPropertyChanged(); } }

        private ViewService _ViewServicesSelected;
        public ViewService ViewServicesSelected { get => _ViewServicesSelected; set { _ViewServicesSelected = value; OnPropertyChanged(); } }

        private StreamModel _ThreadSelected;
        public StreamModel ThreadSelected { get => _ThreadSelected; set { _ThreadSelected = value; OnPropertyChanged(); } }

        private string _APIKey = "10aae7b833b3f36fd943d832260cdffb";
        public string APIKey { get => _APIKey; set { _APIKey = value; OnPropertyChanged(); } }

        private int _Quantity = 0;
        public int Quantity { get => _Quantity; set { _Quantity = value; OnPropertyChanged(); } }
        #endregion

        public event Action<StreamModel, int> NotifyToMain;

        public BuyView()
        {
            InitializeComponent();
            DataContext = this;
            FirstLoad();
        }

        private void FirstLoad()
        {
            LoadThreads();
            LoadViewServiceAsync();
            ViewServicesSelected = ViewServices.FirstOrDefault();
            ThreadSelected = ListThreads.FirstOrDefault();
        }

        private async void LoadViewServiceAsync()
        {
            ViewServices = new ObservableCollection<ViewService>(await HttpRequestHelper.GetServices(APIKey, (o) =>
            {
                MessageBox.Show(o);
            }));
        }

        private void LoadThreads()
        {
            if (System.IO.File.Exists(constants.filePathListThreads))
            {
                var lstItem = File.ReadAllLines(constants.filePathListThreads);
                foreach (var item in lstItem)
                {
                    var data = item.Split('|');

                    StreamModel model = new StreamModel();
                    model.Id = Convert.ToInt32(data.ElementAt(0));
                    model.Title = data.ElementAt(1);
                    model.Input = data.ElementAt(2);
                    model.DateTimeSelected = DateTime.ParseExact(data.ElementAt(3), "dd/MM/yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);
                    model.StreamUrl = data.ElementAt(4);
                    model.StreamKey = data.ElementAt(5);
                    model.Description = data.ElementAt(6);
                    model.Profile = new Profile { ProfileName = data.ElementAt(7) };
                    model.VideoUrl = data.ElementAt(8);

                    ListThreads.Add(model);
                }
            }
        }

        private async void Buy(object sender, RoutedEventArgs e)
        {
            if (ValidateData())
            {
                var videoId = Regex.Match(ThreadSelected.VideoUrl, @"(?<=video/).*?(?=/)", RegexOptions.Singleline).ToString();
                var order = await HttpRequestHelper.AddOrder(APIKey, ViewServicesSelected.service, $"https://www.youtube.com/watch?v={videoId}", Quantity, (o) =>
                {
                    MessageBox.Show(o);
                });
                if (!string.IsNullOrEmpty(order.order))
                {
                    MessageBox.Show($"Mua view thành công. OrderId = {order.order}", "Success");
                    NotifyToMain(ThreadSelected, Quantity);
                }
                else if (!string.IsNullOrEmpty(order.error))
                {
                    MessageBox.Show($"{order.error}", "Alert");
                }
                else
                {

                }
            }
        }

        private bool ValidateData()
        {
            if (ViewServicesSelected == null)
            {
                MessageBox.Show("Nguồn view không được trống!", "Cảnh báo");
                return false;
            }
            if (string.IsNullOrEmpty(ThreadSelected?.VideoUrl))
            {
                MessageBox.Show("Luồng stream chưa tạo được video!", "Cảnh báo");
                return false;
            }
            if (Quantity <= 0)
            {
                MessageBox.Show("Số lượng không hợp lệ!", "Cảnh báo");
                return false;
            }
            if (string.IsNullOrEmpty(APIKey))
            {
                MessageBox.Show("API key không được trống!", "Cảnh báo");
                return false;
            }

            return true;
        }

        private void CancelBuy(object sender, RoutedEventArgs e)
        {

        }
    }
}
