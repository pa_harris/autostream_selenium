﻿using autostream.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace autostream.Helpers
{
    public static class HttpRequestHelper
    {
        static string endpointApi = "https://smmfansfaster.com/api/v2";
        public static async Task<string> RequestPostForm(string endpoint, FormUrlEncodedContent formContent)
        {
            using (HttpClient http = new HttpClient())
            {
                try
                {
                    var response = await http.PostAsync(endpoint, formContent);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responseContent = await response.Content.ReadAsStringAsync();
                        return responseContent;
                    }
                    else
                    {
                        var responseContent = await response.Content.ReadAsStringAsync();
                        return responseContent;
                    }
                }
                catch (Exception ee)
                {

                }
                return null;
            }

        }

        public static async Task<List<ViewService>> GetServices(string apikey, Action<string> callback)
        {
            var formPost = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("key", apikey),
                new KeyValuePair<string, string>("action", "services")
            });


            string res = await HttpRequestHelper.RequestPostForm(endpointApi, formPost);
            if (string.IsNullOrEmpty(res))
            {
                callback(res);
            }
            else
            {
                return JsonConvert.DeserializeObject<List<ViewService>>(res);
            }

            return null;
        }

        public static async Task<Order> AddOrder(string apikey, string serviceId, string linkToPage, int quantity,  Action<string> callback)
        {
            var formPost = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("key", apikey),
                new KeyValuePair<string, string>("action", "add"),
                new KeyValuePair<string, string>("service", serviceId),
                new KeyValuePair<string, string>("link", linkToPage),
                new KeyValuePair<string, string>("quantity", quantity.ToString())
            });


            string res = await HttpRequestHelper.RequestPostForm(endpointApi, formPost);
            if (string.IsNullOrEmpty(res))
            {
                callback(res);
            }
            else
            {
                return JsonConvert.DeserializeObject<Order>(res);
            }

            return null;
        }
    }
}
