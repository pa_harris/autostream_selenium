﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autostream.Helpers
{
    public static class constants
    {
        public static int PreviewMinute { get; set; } = 40;
        public static string profileFolderPath { get; set; } = "Profile";
        public static string filePathListThreads = "ListThreads.txt";
        public static string filePathListProfiles = "ListProfiles.txt";
    }
}
