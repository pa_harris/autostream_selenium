﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace autostream.Models
{
    public class ViewService
    {
        public string service { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string rate { get; set; }
        public string min { get; set; }
        public string max { get; set; }
        public bool dripfeed { get; set; }
        public bool refill { get; set; }
        public string category { get; set; }
    }

    public class Order
    {
        public string order { get; set; }
        public string error { get; set; }
    }
}
