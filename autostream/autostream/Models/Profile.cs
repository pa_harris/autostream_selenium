﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace autostream.Models
{
    public class Profile : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private int _Id;
        public int Id { get => _Id; set { _Id = value; OnPropertyChanged(); } }

        private string _ProfileName;
        public string ProfileName { get => _ProfileName; set { _ProfileName = value; ProfileFullPath = $"Profile\\{value}"; OnPropertyChanged(); } }

        private string _Email;
        public string Email { get => _Email; set { _Email = value; OnPropertyChanged(); } }

        private string _ChannelName;
        public string ChannelName { get => _ChannelName; set { _ChannelName = value; OnPropertyChanged(); } }

        private bool _IsLoggedInOK;
        public bool IsLoggedInOK { get => _IsLoggedInOK; set { _IsLoggedInOK = value; OnPropertyChanged(); } }

        private string _ProfileFullPath;
        public string ProfileFullPath { get => _ProfileFullPath; set { _ProfileFullPath = value; OnPropertyChanged(); } }

        private bool _IsUsing = false;
        public bool IsUsing { get => _IsUsing; set { _IsUsing = value; OnPropertyChanged(); } }

        public string GetDataToFile()
        {
            return $"{Id}|{ProfileName}|{Email}|{ChannelName}|{IsLoggedInOK}";
        }
    }
}
