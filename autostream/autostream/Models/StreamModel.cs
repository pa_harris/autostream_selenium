﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace autostream.Models
{
    public class StreamModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private int _Id;
        public int Id { get => _Id; set { _Id = value; OnPropertyChanged(); } }

        private string _Title;
        public string Title { get => _Title; set { _Title = value; OnPropertyChanged(); } }

        private string _Input;
        public string Input { get => _Input; set { _Input = value; OnPropertyChanged(); } }

        private double _Bitrate;
        public double Bitrate { get => _Bitrate; set { _Bitrate = value; OnPropertyChanged(); } }

        private EServer _Server;
        public EServer Server { get => _Server; set { _Server = value; OnPropertyChanged(); } }

        private string _StreamUrl;
        public string StreamUrl { get => _StreamUrl; set { _StreamUrl = value; OnPropertyChanged(); } }

        private string _Description;
        public string Description { get => _Description; set { _Description = value; OnPropertyChanged(); } }

        private string _StreamKey;
        public string StreamKey { get => _StreamKey; set { _StreamKey = value; OnPropertyChanged(); } }

        private double _Speed;
        public double Speed { get => _Speed; set { _Speed = value; OnPropertyChanged(); } }

        private int _FPS;
        public int FPS { get => _FPS; set { _FPS = value; OnPropertyChanged(); } }

        private Profile _Profile;
        public Profile Profile { get => _Profile; set { _Profile = value; ; OnPropertyChanged(); } }

        private string _VideoUrl;
        public string VideoUrl { get => _VideoUrl; set { _VideoUrl = value; OnPropertyChanged(); } }

        private int _BuyView = 0;
        public int BuyView { get => _BuyView; set { _BuyView = value; OnPropertyChanged(); } }

        private DateTime _DateTimeSelected = DateTime.Now;
        public DateTime DateTimeSelected { get => _DateTimeSelected; set { _DateTimeSelected = value; OnPropertyChanged(); } }

        private DateTime _DateTimeEndSelected = DateTime.Now;
        public DateTime DateTimeEndSelected { get => _DateTimeEndSelected; set { _DateTimeEndSelected = value; OnPropertyChanged(); } }

        private EStatus _Status = EStatus.None;
        public EStatus Status { get => _Status; set { _Status = value; OnPropertyChanged(); } }

        public Thread thread { get; set; }
        public ChromeDriver chromeDriver { get; set; }
        public Process cmd { get; set; }

        public string GetDataToFile()
        {
            return $"{Id}|{Title}|{Input}|{DateTimeSelected.ToString("dd/MM/yyyy hh:mm:ss tt")}|{StreamUrl}|{StreamKey}|{Description}|{Profile.ProfileName}|{VideoUrl}|{BuyView}|";
        }
    }
}
